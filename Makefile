ARCH=arm-none-eabi-
CC=$(ARCH)gcc
LD=$(ARCH)ld
OBJDUMP=$(ARCH)objdump
CFLAGS=-O0 -g -nostdlib
LDFLAGS=-nostdlib -g

GOALS=ln ln_complex

all: $(GOALS)

%.o : %.c

% : %.o

clean:
	rm -f *.csv *.o $(GOALS)
