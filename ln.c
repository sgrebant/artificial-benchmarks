/**
 * This artificial benchmark can stress the polyhedra analysis In
 * fact, the analysis analyzes several times each loop body until a
 * fixpoint is reached. Thus this benchmark nests loops on several
 * depth to demonstrate that modular analysis is way faster in this
 * case
 */

/* Sample function that performs computations */
int compute(int x, int y, int z, int a){
  int res = 0;
  res += x;
  res += y;
  res += z;
  res += a;
  return res;
}

/* no loop */
int ln0(){
  int res = 0;
  res = compute(0,0,0,0);
  return res;
}

/* loop nest l */
int ln1(int x){
  int res = 0;
  for(int i = 0; i < x; i++)
    res += compute(i,0,0,0);
}

/* loop nest l > l */
int ln2(int x){
  int res = 0;
  for(int i = 0; i < x; i++)
    for(int j = 0; j < x; j++)
      res += compute(i,j,0,0);
  return res;
}

/* loop nest l > l > l */
int ln3(int x){
  int res = 0;
  for(int i = 0; i < x; i++)
    for(int j = 0; j < x; j++)
      for(int k = 0; k < x; k++)
	res += compute(i,j,k,0);
  return res;
}
/* loop nest l > l > l > l */
int ln4(int x){
  int res = 0;
  for(int i = 0; i < x; i++)
    for(int j = 0; j < x; j++)
      for(int k = 0; k < x; k++)
	for(int l = 0; l < x; l++)
	  res += compute(i,j,k,l);
}

/*  Sample main function */
int main(){
  ln0();
  ln1(10);
  ln2(10);
  ln3(10);
}
